require_relative "docentes.rb"
require_relative "discentes.rb"

include Discente
include Docentes


//Criando
GMA = Departamento.new(["Calc 1", "Calc 2", "Alg Lin"], "H", "GMA")
Marco = Professor.new(3000, "Marco", 324324, 70, GMA)

Joao = Aluno.new("Joao", 15445, "CC", 23, 9)

module Bd

    class BancoDeDados
        def initialize(nomeBd)
            @nome = nomeBd
            @array = []
        end

        def bancoPush(item)
            @array.push(item)
        end

        def mostrarBanco
            puts @array
        end

    end

end

Banco =  Bd::BancoDeDados.new("Algum")

Banco.bancoPush(Marco.hash)
Banco.bancoPush(Joao.hash)
Banco.mostrarBanco