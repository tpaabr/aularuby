 module Docentes

    class Professor
        def initialize(salarioProf, nomeProf, matProf, idadeProf, depProf)

            @salario = salarioProf
            @nome = nomeProf
            @matricula = matProf
            @idade = idadeProf
            @departamento = depProf

        end

        def hash
            {salario: @salario, nome: @nome, matricula: @matricula, idade: @idade, dep: @departamento}
        end

    end

    class Departamento 
        def initialize(materiasDep, predioDep, siglaDep)

            @materias = materiasDep
            @predio = predioDep
            @sigla = siglaDep
        end

    end

end