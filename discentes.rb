module Discente
    class Aluno
        def initialize(nomeAluno, matriculaAluno, cursoAluno, idadeAluno) 
            @nome = nomeAluno
            @matricula =  matriculaAluno
            @curso = cursoAluno
            @idade = idadeAluno
            @cr = crAluno

        end

        def hash
            {cr: @cr, nome: @nome, matricula: @matricula, idade: @idade}
        end

    end

    class AlunoGraduacao < Aluno

        def initialize(crAluno, cargaAluno)
            @nome = nomeAluno
            @matricula =  matriculaAluno
            @curso = cursoAluno
            @idade = idadeAluno
            @cr = crAluno
            @cargaHoraria = cargaAluno
        end

        def hash
            {carga_aluno: @cargaHoraria ,cr: @cr, nome: @nome, matricula: @matricula, idade: @idade}
        end

    
    end

    class AlunoMestrado < Aluno

        def initialize(nomeAluno, matriculaAluno, cursoAluno, idadeAluno, gradAluno, orientadorAluno, areaAluno)
            @nome = nomeAluno
            @matricula =  matriculaAluno
            @curso = cursoAluno
            @idade = idadeAluno
            @graduacao = gradAluno
            @orientador = orientadorAluno
            @area_pesquisa = areaAluno
        end

        
        def hash
            {area_pesquisa: @area_pesquisa ,orientador: @orientador, graduacao: @graduacao, nome: @nome, matricula: @matricula, idade: @idade}
        end

    end

end